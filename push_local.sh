#!/usr/bin/bash
git add .
git log --oneline | head -5 | bat
echo "Write down the commit name:"
read -p "> " name
git commit -m "$name"
git push
git log --oneline | head -5 | bat
