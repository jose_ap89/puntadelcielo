from django.urls import path

from . import views

app_name = 'puntadelcielo'
urlpatterns = [
    path('', views.index, name='index'),
    path('instalaciones/', views.facilities, name='facilities'),
    path('contacto/', views.contact, name='contact'),
    path('preguntas/', views.questions, name='questions'),
]

