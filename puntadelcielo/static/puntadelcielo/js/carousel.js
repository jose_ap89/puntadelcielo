document.querySelectorAll(".mySlides").forEach( function(_,i){
    let dot = document.createElement('span'); 
    dot.className = "dot";
    dot.style.cssText = "width:20px;height:27px;";
    dot.setAttribute("onclick", `currentSlide(${i+1})`);
    document.querySelector("#dotContainer").append(dot);
});

let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    let i;
    let slides = document.getElementsByClassName("mySlides");
    let dots = document.getElementsByClassName("dot");
    if (n > slides.length) {slideIndex = 1}    
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";  
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";  
    if(dots.length>0) {
        dots[slideIndex-1].className += " active";
    }
}
