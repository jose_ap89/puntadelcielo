let questions = document.querySelectorAll(".slidedown");
const darr = "&ensp;<i class='fa fa-angle-double-up'></i>";
const rarr = "&ensp;<i class='fa fa-angle-double-down'></i>";

// expands-contracts the question according to the event triggered 
["mouseenter","click"].forEach( evt => {
    questions.forEach(function(question,i){
        let trigger = question.querySelector(".slidedown-trigger .question");
        let menu = question.querySelector(".slidedown-menu");
        let icon = question.querySelector(".slidedown-icon")
        trigger.addEventListener( evt, function() {
            // if it is not open
            if(menu.classList.contains("is-hidden")){
                menu.classList.toggle("is-hidden");   
                icon.innerHTML = darr;
            // if it is open
            } else {
                menu.classList.toggle("is-hidden");   
                icon.innerHTML = rarr;
            }
            for(let j=0;j<questions.length;j++){
                let menu = questions[j].querySelector(".slidedown-menu");
                let icon = questions[j].querySelector(".slidedown-icon");
                if(j!=i){
                    menu.classList.add("is-hidden");   
                    icon.innerHTML = rarr;
                }
            }
        });
    });
});
