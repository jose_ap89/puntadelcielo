let pics = document.querySelectorAll("div figure img.zoom");
let counterPics = Array.from(Array(pics.length)).map(x => false);

// expands-contracts the pic according to the event triggered 
["mouseover","mouseout","click"].forEach( evt => {
    pics.forEach(function(pic,i){
        pic.addEventListener( evt, function() {
            counterPics[i] = !counterPics[i];
            if(counterPics[i]){
                pic.classList.add("expandPic");    
            } else {
                pic.classList.remove("expandPic");    
            }
            for(let j=0;j<pics.length;j++){
                if(evt === "mouseout" || j!=i){
                    counterPics[j] = false;
                    pics[j].classList.remove("expandPic");
                }
            }
        });
    });
});

/* overlay functionality */
function openNav() {
    document.getElementById("myNav").style.width = "100%";
}
  
function closeNav() {
    document.getElementById("myNav").style.width = "0%";
}

/* modal functionality  */

let modal = document.querySelector("#weatherModal")
function getWeather() {
    modal.classList.add("is-active");
}

function dropWeather(){
    modal.classList.remove("is-active");
}


