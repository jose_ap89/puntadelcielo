from .models import WeatherRequest  as WR
from django.utils import timezone as tz
from datetime import datetime as dt, timedelta
from .wind import Wind
import json
import requests
import pytz
import copy

# OPENWEATHER API = OWAPI
# DB = DATABASE

class WeatherMap:
    '''https://openweathermap.org/'''
    latitude = 20.4764322
    longitude = -97.006642
    appikey = "5af0042005116596d75564e0d405e625"
    request_dt : 'DateTimeLocale' = None
    data : 'Dict<any>' = None 

    def __init__(self):
        '''All datetime saved in the DB is in utc format and used in localtime format
            Datetime saved in memory is in localtime format   
        '''
        weather_reqs = WR.objects.all()
        if len( weather_reqs )== 0 :
            print("........WR first update........")
            # first request to api gets its datetime and data to be the first ones put into the DB
            wr = WR(last_updated = tz.now() )
            WeatherMap.request_dt = wr.localtime 
            WeatherMap.data = self.fetch_data()
            wr.data = json.dumps(WeatherMap.data)  
            wr.save()
        else :
            # if there is a date and data in the DB already then just assign them to the class variables
            WeatherMap.request_dt = weather_reqs[0].localtime
            WeatherMap.data = json.loads(weather_reqs[0].data) 
        self.curr_dt = tz.localtime(tz.now())

    def get_data(self) -> 'Dict<any>':    
        ''' Returns a dict with data fetched from OWAPI if the response was a 2xx status 
            If there is a difference of 1 h between last_updated from the DB and curr_dt from the client time, then
            it will fetch new data from OWAPI and will return a dict if the response is ok, otherwise an empty dict 
            if there was a bad response from OWAPI then there is an empty dict in data in the DB, so two scenarios:
                - time passed between calls is superior than 0.16667h=10min, reset_time_error, then new data from OWAPI is fetched again
                - time passed between calss is less than 10 min then no new fetch is done and an empty dict is returned
        '''
        diff = WeatherMap.delta_hours(self.curr_dt, self.request_dt)
        reset_time_error = 0.16667
        if  diff > 1.0 or ( len(self.data)==0 and diff > reset_time_error ):
            # if there is a difference between the date in the DB and the current client datetime > 1 h then
            # update datetime and data in the DB and in the class variable request_dt
            # or if there was a bad response from and there is a difference in 0.16667h = 10min
            # then try again sending a get request to openweather api OWAPI
            self.request_dt = self.curr_dt
            weather_req = WR.objects.all()[0]
            weather_req.last_updated = tz.now()
            self.data = self.fetch_data()
            weather_req.data = json.dumps(self.data)
            weather_req.save()
        data = copy.deepcopy(self.data)   
        for k,val in self.data.items():
            if k=="current":
                for k2,val2 in val.items():
                    if k2 == "sunset" or k2 == "sunrise" or k2 == "dt":
                        if k2 == "dt": data[k][k2] = dt.strptime( val2, "%Y-%m-%d %H:%M:%S")
                        else:  data[k][k2] = dt.strptime( val2, "%H:%M:%S").time()
                    if k2 == "wind_deg":
                        data[k]["wind_dir"] = Wind.wind_dir(val2) # it converts degrees into wind directions like N S E O
                    if k2 == "wind_speed":
                        data[k]["wind_scale"] = Wind.wind_scale(val2) # it converts wind speed into Beaufort scale
            if k=="daily":
                for i,day in enumerate(val):
                    for k2,val2 in day.items():
                        if k2 == "sunset" or k2 == "sunrise" or k2 == "dt":
                            if k2 == "dt": data[k][i][k2] = dt.strptime( val2, "%Y-%m-%d %H:%M:%S")
                            else:  data[k][i][k2] = dt.strptime( val2, "%H:%M:%S").time()
                        if k2 == "wind_deg":
                            data[k][i]["wind_dir"] = Wind.wind_dir(val2) # it converts degrees into wind directions like N S E O
                        if k2 == "wind_speed":
                            data[k][i]["wind_scale"] = Wind.wind_scale(val2) # it converts wind speed into Beaufort scale

        return data

    @staticmethod
    def delta_hours(new: 'datetime', old: 'datetime') -> 'f64':
        hours = (new - old).seconds / 3600.0
        return hours

    def fetch_data(self) -> 'Dict<any>':
        ''' It fetches data from OWAPI and returns a dict  
            If there is a bad response from OWAPI then the error is handled returning an empty dict
        '''
        print("........FETCHING NEW WEATHER DATA........")
        print("........NEW TIME FOR UPDATING........")
        url = f"https://api.openweathermap.org/data/2.5/onecall?lat={self.latitude}&lon={self.longitude}&exclude=hourly,minutely,alerts&lang=sp&units=metric&appid={self.appikey}"
        data = None 
        try:
            data = requests.get(url).json()
        except :
            data = dict()
            print("........There was a mistake with the get request to openweathermap api........")
            print("data = ",data)
            return data
        WeatherMap.transform(data)
        print(f">> {self.request_dt} | {data['current']['temp']}°C")
        return data

    @staticmethod
    def transform(data: '&mut Dict<any>'):
        ''' It converts timestamp digit points, that represent datetime in a format of seconds, into datetime type
            It makes the necessary unit convertions
        '''
        def unix_to_datetime(value: 'i32') -> 'datetime':
            datetime = dt.fromtimestamp(value)
            datetime = f"{datetime.year:02}-{datetime.month:02}-{datetime.day:02}T{datetime.hour:02}:{datetime.minute:02}:{datetime.second:02}"
            return datetime
        for k,val in data.items():
            if k=="current":
                for k2,val2 in val.items():
                    if k2 == "sunset" or k2 == "sunrise" or k2 == "dt":
                        res = unix_to_datetime(val2).split("T")
                        if k2 == "dt": data[k][k2] = " ".join(res)
                        else:  data[k][k2] = res[1]
                    if k2 == "wind_speed":
                        data[k][k2] *= 3.6 # to convert m/s to k/h
            if k=="daily":
                for i,day in enumerate(val):
                    for k2,val2 in day.items():
                        if k2 == "sunset" or k2 == "sunrise" or k2 == "dt":
                            res = unix_to_datetime(val2).split("T")
                            if k2 == "dt": data[k][i][k2] = " ".join(res)
                            else:  data[k][i][k2] = res[1]
                        if k2 == "wind_speed":
                            data[k][i][k2] *= 3.6 # to convert m/s to k/h
                        if k2 == "pop":
                            data[k][i][k2] *= 100 # to convert decimal probability to percentage

    def __str__(self):
        return f"request_dt:({self.request_dt}), curr_dt:({self.curr_dt})"
