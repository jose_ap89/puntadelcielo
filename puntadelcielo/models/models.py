from django.db import models
from django.utils import timezone as tz

class WeatherRequest(models.Model):
    last_updated = models.DateTimeField()
    data = models.JSONField()
    @property
    def localtime(self):
        return tz.localtime(self.last_updated)
    def __str__(self):
        return f"({self.localtime})"
