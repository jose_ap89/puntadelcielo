class Wind:
    @staticmethod
    def wind_dir(degrees:'i32') -> "String":
        if degrees == 0 or degrees == 360:
            return "N"
        elif degrees == 45 :
            return "NE"
        elif degrees < 45 :
            return "NNE"
        elif degrees == 90 :
            return "E"
        elif degrees < 90 :
            return "ENE"
        elif degrees == 135 :
            return "SE"
        elif degrees < 135 :
            return "ESE"
        elif degrees == 180 :
            return "S"
        elif degrees < 180 :
            return "SSE"
        elif degrees == 225 :
            return "SO"
        elif degrees < 225 :
            return "SSO"
        elif degrees == 270 :
            return "O"
        elif degrees < 270 :
            return "OSO"
        elif degrees == 315 :
            return "NO"
        elif degrees < 315 :
            return "ONO"
        else:
            return "NNO"

    @staticmethod
    def wind_scale(wind:'i32') -> "String":
        ''' It gives the wind Beaufort scale'''
        if wind < 2 :
            return "Calmado(0)"
        elif wind <= 5:
            return "Aire ligero(1)"
        elif wind <= 11:
            return "Briza ligera(2)"
        elif wind <= 19:
            return "Briza suave(3)"
        elif wind <= 28:
            return "Briza moderada(4)"
        elif wind <= 38:
            return "Briza fresca(5)"
        elif wind <= 49:
            return "Briza fuerte(6)"
        elif wind <= 61:
            return "Cerca de Vendaval(7)"
        elif wind <= 74:
            return "Vendaval(8)"
        elif wind <= 88:
            return "Vendaval fuerte(9)"
        elif wind <= 102:
            return "Tormenta(10)"
        elif wind <= 117:
            return "Tormenta violenta(11)"
        else:
            return "Huracán(12)"
