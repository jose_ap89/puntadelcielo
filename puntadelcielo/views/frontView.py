from django.shortcuts import render, redirect
from django.http import HttpResponse
from ..models.weatherMap import WeatherMap

def index(request):
    weather = WeatherMap()
    context = {
        "weather" : weather.get_data()
    }
    if request.method == "GET":
        #return render("index.html",context)
        return render(request,"puntadelcielo/index.html", context) 

def facilities(request):
    weather = WeatherMap()
    context = {
        "weather" : weather.get_data(),
        "pictures": [
            {
                "name": "pool.webp",
                "caption": "Piscina"
            },
            {
                "name": "room.webp",
                "caption": "Habitación vista 1"
            },
            {
                "name": "roomview2.webp",
                "caption": "Habitación vista 2"
            },
            {
                "name": "roomview3.webp",
                "caption": "Habitación vista 3"
            },
            {
                "name": "restroomview1.webp",
                "caption": "Baño vista 1"
            },
            {
                "name": "restroomview2.webp",
                "caption": "Baño vista 2"
            }
        ]
    }
    if request.method == "GET":
        return render(request,"puntadelcielo/facilities.html",context) 

def questions(request):
    weather = WeatherMap()
    context = {
        "weather" : weather.get_data(),
        "questions": [
            {
                "question":"¿Cuánto cuesta rentar una habitación?",
                "answer": "Precio esta entre los $550 y $650 dependiendo si es temporada turística alta o baja"
            },
            {
                "question":"¿Qué fecha es el festival del coco en Tecolutla?",
                "answer": "Finales de Febrero y principios de Marzo"
            },
            {
                "question":"¿Qué tan lejos queda la playa del hotel?",
                "answer": "Esta a 4 cuadras, o aproximadamente unos 4 minutos caminando"
            },
            {
                "question":"¿Qué tan lejos queda el río de las instalaciones?",
                "answer": "Se encuentra a 280 metros o 3 minutos a pie"
            }
        ]
    }
    if request.method == "GET":
        return render(request,"puntadelcielo/qa.html",context) 

def contact(request):
    weather = WeatherMap()
    context = {
        "weather" : weather.get_data()
    }
    if request.method == "GET":
        return render(request,"puntadelcielo/contact.html", context) 
