from django.contrib import admin
from django.urls import include, path

app_name = 'puntadelcielo'
urlpatterns = [
    path('', include('puntadelcielo.urls')),
]
